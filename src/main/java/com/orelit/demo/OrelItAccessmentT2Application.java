package com.orelit.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrelItAccessmentT2Application {

	public static void main(String[] args) {
		SpringApplication.run(OrelItAccessmentT2Application.class, args);
	}

}
